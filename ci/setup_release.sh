#!/bin/bash

function contact_author {
 author_name='Ionut Nechita'
 author_email='ionut_n2001@yahoo.com'
 author_website='ionutnechita.ro'
 author_city='Galati'
 author_country='Romania'
 author_license='AGPL v3'
 author="$author_name $author_email $author_website $author_city $author_country $author_license"
 echo $author
}

function description_5607 {
  echo " This script is used to download the latest version of json for kernel versions and to create the environment for these versions."
  echo " Use environment variable for more display and create file for start new environment: VIEW_CSV, CREATE_ENV"
}

function display_welcome {
 clear
 echo -ne "\n =^=^=^=^=^=^=^=^=^=^= WELCOME ! $(date) \n\n\
 $(contact_author)\n\n"
 sleep .5
 echo "
    devops2019
    Copyright (C) 2019 Ionut Nechita RO
 "
}

function save_json_on_file {
 path='.'
 json_name='releases.json'
 url_name='https://www.kernel.org/releases.json'
 echo -ne " Downloading the latest json file with the kernel versions.\n
 $url_name downloading on $path/$json_name ...\n\
 ==== if the file does not exist, the process will stop. ====\n\n"
 wget -O $path/$json_name $url_name || exit 1
 echo " File downloaded on $path/$json_name."
}

function count_list_kernel {
 local counter=$(cat $path/$json_name | jq -r ".releases[] | .$1" | wc -l)
 echo $counter
}

function show_param_kernel {
 local param=$(cat $path/$json_name | jq -r ".releases[$2] | .$1")
 echo $param
}

function create_csv_file {
 csv_name='releases.csv'
 >$path/$csv_name
 echo " Create new csv file for all releases."
 cat $path/$csv_name
 for (( i=0; i<$(count_list_kernel 'version'); i++ ));
 do
  echo "$(show_param_kernel 'version' $i),$(show_param_kernel 'moniker' $i),$(show_param_kernel 'source' $i)" >> $path/$csv_name
 done
 if [[ $VIEW_CSV == "1" ]];
 then
  cat $path/$csv_name
 fi
}

function detect_moniker {
 local moniker=$1
 case $moniker in
 stable) echo " Search new list for stable kernel."
 ;;
 longterm) echo " Search new list for longterm kernel."
 ;;
 mainline) echo " Search new list for mainline kernel."
 ;;
 linux-next) echo " Search new list for linux-next kernel."
 ;;
 *) echo " Parameter issue. Uses one of the following parameters:\
    stable, longterm, mainline, linux-next."
    echo " It's closing."
    exit 1
 ;;
 esac
}

function create_environment {
 if [[ $CREATE_ENV == "1" ]];
 then
  local name_kernel=$(echo $line | cut -d, -f1)
  local moniker=$(echo $line | cut -d, -f2)
  local source=$(echo $line | cut -d, -f3)
  >$path/$moniker-$name_kernel.ini
  echo "export KERNEL=$name_kernel" >>$path/$moniker-$name_kernel.ini
  echo "export MONIKER=$moniker" >>$path/$moniker-$name_kernel.ini
  echo "export SOURCE_KERNEL=$source" >>$path/$moniker-$name_kernel.ini
 fi
}

function view_kernel {
 moniker=$1
 detect_moniker $moniker
 while read line; do
 if [[ "x$(echo $line | grep $moniker)" != "x" ]]
 then
  echo $line
  create_environment $line
 fi
 done < $path/$csv_name
}

function setup_releases {
 display_welcome
 description_5607
 echo -ne '######                    (25%)\r'
 sleep 1
 save_json_on_file
 echo -ne '############              (50%)\r'
 sleep 1
 create_csv_file
 echo -ne '##################        (75%)\r'
 sleep 1
 view_kernel $1
 echo -ne '########################  (100%)\r'
 echo -ne '\n'
 sleep 1
}


setup_releases $1
